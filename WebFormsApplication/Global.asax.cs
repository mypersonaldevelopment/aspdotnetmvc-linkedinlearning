﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebFormsApplication
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Application["ApplicationStartDateTime"] = DateTime.Now;     // Application variable to store the application start time
        }

        // Added the Session Variable to keep track of the Session start date and time
        void Session_Start(object sender, EventArgs e) {
            Session["SessionStartDateTime"] = DateTime.Now;
        }
    }
}