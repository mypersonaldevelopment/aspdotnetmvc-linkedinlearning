﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewsletterSignUp.aspx.cs" Inherits="WebFormsApplication.NewsletterSignUp" ViewStateMode="Disabled" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Newsetter Signup</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Newsletter Signup</h1>
            <p>
                <asp:Literal ID="ltCaption" runat="server"></asp:Literal>
            </p>
            <p>
                <asp:Label ID="lblEmail" runat="server" Text="Enter e-mail"></asp:Label>
&nbsp;&nbsp;
                <asp:TextBox ID="txtEmail" runat="server" ViewStateMode ="Disabled"></asp:TextBox>
            </p>
            <p>&nbsp;</p>
            <p>
                <asp:Button ID="btnSubscribe" runat="server" OnClick="btnSubscribe_Click" Text="Subscribe" />
            </p>
        </div>
    </form>
</body>
</html>
